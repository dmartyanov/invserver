package ru.tcsbank.system

import akka.actor.{Actor, ActorLogging}
import ru.tcsbank.api.ApiHttpService
import ru.tcsbank.utils.context.ExecutionContextComponent

/**
 * Created by d.a.martyanov on 24.12.14.
 */
class ServiceActor extends Actor with ActorLogging
with ApiHttpService {
  this: ExecutionContextComponent =>

  def actorRefFactory = context

  override def receive: Receive = runRoute(serviceRoute)
}
