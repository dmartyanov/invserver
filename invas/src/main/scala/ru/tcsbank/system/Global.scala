package ru.tcsbank.system

import java.util.concurrent.{LinkedBlockingDeque, TimeUnit, ThreadPoolExecutor}

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import ru.tcsbank.api.controller.{GameReplayController, StaticResourceController}
import ru.tcsbank.dao.conn.{TransactionDBManager, MySQLDatabaseComponent, DatabaseHelper, MySQLDatabaseManager}
import ru.tcsbank.utils.as.ActorSystemComponent
import ru.tcsbank.utils.config.ConfigHelper
import ru.tcsbank.utils.context.ExecutionContextComponent
import ru.tcsbank.utils.json.JsonHelper
import ru.tcsbank.utils.logging.LoggerHelper

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

/**
 * Created by d.a.martyanov on 24.12.14.
 */
object Global {

  private lazy val configImpl = ConfigFactory.load()

  trait ConfigurationImpl extends ConfigHelper {
    override lazy val config = configImpl
  }

  private lazy val actors = ActorSystem("invasas", configImpl)

  trait ActorSystemComponentImpl extends ActorSystemComponent {
    override implicit def actorSystem: ActorSystem = actors
  }

  trait DefaultExecutionContextComponentImpl extends ExecutionContextComponent {
    override implicit def executionContext: ExecutionContext = actors.dispatcher
  }

  lazy val pooledDB = new MySQLDatabaseManager with LoggerHelper with ConfigurationImpl
  trait MySQLDatabase extends DatabaseHelper {
    override val db: MySQLDatabaseComponent = pooledDB
  }

  lazy val dataBaseRequestExecutionPool = ExecutionContext.fromExecutorService(
    new ThreadPoolExecutor(
      Runtime.getRuntime.availableProcessors(),
      16, (3 minute) toMillis,
      TimeUnit.MILLISECONDS,
      new LinkedBlockingDeque[Runnable]()
    )
  )
  trait DatabaseExecutionContext extends ExecutionContextComponent{
    override implicit def executionContext: ExecutionContext = dataBaseRequestExecutionPool
  }

  lazy val mySQLTransactionManager = new TransactionDBManager with DatabaseExecutionContext with MySQLDatabase

  val resourcesPath = configImpl.getString("staticResourcePath")
  lazy val staticSprayController = new StaticResourceController("static", resourcesPath) with ActorSystemComponentImpl

  lazy val replayController = new GameReplayController with DefaultExecutionContextComponentImpl
    with LoggerHelper with JsonHelper

  lazy val unibotDir = configImpl.getString("unibotLocation")
  lazy val randombotDir = configImpl.getString("randombotLocation")
}
