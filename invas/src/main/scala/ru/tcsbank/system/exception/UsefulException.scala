package ru.tcsbank.system.exception

/**
 * Created by d.a.martyanov on 24.12.14.
 */
trait UsefulException {
  def cause: Option[Throwable]

  def description: Option[String]

  def message: String

  def title: String
}

object UsefulExceptionRegistry {
  val CLIENT_REQUEST_EXCEPTION = "ClientRequestException"
  val LADDER_MANAGER_EXCEPTION = "LadderManagerException"
  val ARENA_EXCEPTION = "AreanException"
  val GALAXY_EXCEPTION = "GalaxyException"
  val DB_ACCESS_EXCEPTION = "DBAccessException"
  val UI_REPLAY_EXCEPTION = "ReplayException"
}
