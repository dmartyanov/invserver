package ru.tcsbank.system.exception

/**
 * Created by d.a.martyanov on 29.12.14.
 */
class UIGameReplayException(msg: String, cauze: Option[Throwable] = None, descr: Option[String] = None)
  extends InvasException(msg, cauze, descr) {
  override def title: String = UsefulExceptionRegistry.DB_ACCESS_EXCEPTION
}
