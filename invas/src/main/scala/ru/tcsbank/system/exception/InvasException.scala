package ru.tcsbank.system.exception

/**
 * Created by d.a.martyanov on 24.12.14.
 */
abstract class InvasException(msg: String, cauze: Option[Throwable] = None, descr: Option[String] = None)
  extends Exception(msg, if (cauze.isDefined) cauze.get else null)
  with UsefulException {

  override def cause: Option[Throwable] = cauze

  override def description: Option[String] = descr

  override def message: String = msg
}
