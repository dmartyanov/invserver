package ru.tcsbank.system

import akka.actor.Props
import akka.io.IO
import ru.tcsbank.system.Global.{ActorSystemComponentImpl, ConfigurationImpl, DefaultExecutionContextComponentImpl}
import spray.can.Http
import spray.routing.SimpleRoutingApp

/**
 * Created by d.a.martyanov on 24.12.14.
 */
object Main extends App
with SimpleRoutingApp
with ActorSystemComponentImpl
with ConfigurationImpl {

  val handler = actorSystem.actorOf(
    Props(new ServiceActor with DefaultExecutionContextComponentImpl)
    , name = "handler"
  )

  IO(Http) ! Http.Bind(handler, interface = "0.0.0.0", port = config.get[Int]("app.port", 9000))
}
