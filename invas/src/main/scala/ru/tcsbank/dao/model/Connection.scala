package ru.tcsbank.dao.model

import scala.slick.driver.MySQLDriver.simple._
import scala.slick.lifted.ProvenShape

/**
 * Created by d.a.martyanov on 22.12.14.
 */
case class Connection(
                       ticket: String,
                       userId: String,
                       creationDate: Long,
                       closed: Int,
                       address: String
                       )

case class Connections(tag: Tag) extends Table[Connection](tag, "connection"){
  def ticket = column[String]("ticket", O.PrimaryKey)

  def userId = column[String]("user_id", O.NotNull)

  def creationDate = column[Long]("creation_date", O.NotNull)

  def closed = column[Int]("closed", O.NotNull)

  def address = column[String]("address", O.NotNull)

  override def * : ProvenShape[Connection] =
    (ticket, userId, creationDate, closed, address) <> (Connection.tupled, Connection.unapply)

  def user = foreignKey("fk_connection_user_id", userId, TableQuery[Users])(_.userId)
}
