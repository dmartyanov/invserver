package ru.tcsbank.dao.model

import scala.slick.driver.MySQLDriver.simple._
import scala.slick.lifted.ProvenShape

/**
 * Created by d.a.martyanov on 22.12.14.
 */
case class GameResult(
                       resultId: String,
                       galaxy: String,
                       playersResult: String,
                       rounds: Int,
                       winnerId: String
                       )

class GameResults(tag: Tag) extends Table[GameResult](tag, "gameresult") {

  def resultId = column[String]("result_id", O.PrimaryKey)

  def galaxy = column[String]("galaxy", O.NotNull)

  def playersResult = column[String]("players_result", O.NotNull)

  def rounds = column[Int]("rounds", O.NotNull)

  def winnerId = column[String]("winner_id", O.NotNull)

  override def * : ProvenShape[GameResult] =
    (resultId, galaxy, playersResult, rounds, winnerId) <>(GameResult.tupled, GameResult.unapply)

  def winner = foreignKey("fk_gameresult_user_id", winnerId, TableQuery[Users])(_.userId)

}