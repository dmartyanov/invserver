package ru.tcsbank.dao.model

import scala.slick.driver.MySQLDriver.simple._
import scala.slick.lifted.ProvenShape

/**
 * Created by d.a.martyanov on 22.12.14.
 */
case class User(
                 id: String,
                 login: Option[String] = None,
                 rating: Int
                 )

class Users(tag: Tag) extends Table[User](tag, "user") {
  def userId = column[String]("user_id", O.PrimaryKey)

  def login = column[String]("login")

  def rating = column[Int]("rating", O.NotNull)

  override def * : ProvenShape[User] = (userId, login.?, rating) <>(User.tupled, User.unapply)
}