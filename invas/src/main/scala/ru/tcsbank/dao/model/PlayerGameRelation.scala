package ru.tcsbank.dao.model

import scala.slick.lifted.ProvenShape
import slick.driver.MySQLDriver.simple._

/**
 * Created by d.a.martyanov on 23.12.14.
 */
case class PlayerGameRelation(
                               gameId: String,
                               userId: String,
                               ticket: String
                               )

class PlayerGameRelations(tag: Tag) extends Table[PlayerGameRelation](tag, "gamesplayers"){
  def gameId = column[String]("game_id", O.NotNull)

  def userId = column[String]("user_id", O.NotNull)

  def ticketId = column[String]("ticket", O.NotNull)

  override def * : ProvenShape[PlayerGameRelation] =
    (gameId, userId, ticketId) <> (PlayerGameRelation.tupled, PlayerGameRelation.unapply)

  def game = foreignKey("fk_gamesplayers_game_id", gameId, TableQuery[Games])(_.gameId)

  def user = foreignKey("fk_gamesplayers_user_id", userId, TableQuery[Users])(_.userId)

  def ticket = foreignKey("fk_gamesplayers_connection_id", ticketId, TableQuery[Connections])(_.ticket)
}
