package ru.tcsbank.dao.model

import scala.slick.driver.MySQLDriver.simple._
import scala.slick.lifted.ProvenShape

/**
 * Created by d.a.martyanov on 22.12.14.
 */
case class GameLog(
                    gameId: String,
                    round: Int,
                    moves: String,
                    galaxy: String,
                    last: Int
                    )

class GameLogs(tag: Tag) extends Table[GameLog](tag, "gamelog") {
  def gameId = column[String]("game_id", O.NotNull)

  def round = column[Int]("round", O.NotNull)

  def moves = column[String]("moves", O.NotNull)

  def galaxy = column[String]("resultgalaxy", O.NotNull)

  def last = column[Int]("last", O.NotNull)

  override def * : ProvenShape[GameLog] = (gameId, round, moves, galaxy, last) <>(GameLog.tupled, GameLog.unapply)

  def game = foreignKey("fk_gamelog_game_id", gameId, TableQuery[Games])(_.gameId)
}