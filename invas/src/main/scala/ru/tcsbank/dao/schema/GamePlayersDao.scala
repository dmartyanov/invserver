package ru.tcsbank.dao.schema

import ru.tcsbank.dao.model.{PlayerGameRelation, PlayerGameRelations}

import slick.driver.MySQLDriver.simple._

/**
 * Created by d.a.martyanov on 23.12.14.
 */
object GamePlayersDao {

  import ru.tcsbank.system.Global.mySQLTransactionManager._

  val plGameRel = TableQuery[PlayerGameRelations]

  def list = tx { implicit session => plGameRel.list }

  def insert(pgRel: PlayerGameRelation) = tx { implicit session => plGameRel += pgRel }

  def playerGames(uid: String) = tx { implicit session => plGameRel.filter(_.userId === uid).list }

  def gamePlayers(gid: String) = tx { implicit session => plGameRel.filter(_.gameId === gid).map(_.userId).list}
}
