package ru.tcsbank.dao.schema

import ru.tcsbank.dao.model.{Connection, Connections}
import ru.tcsbank.system.exception.DBAccessException

import scala.slick.driver.MySQLDriver.simple._

/**
 * Created by d.a.martyanov on 22.12.14.
 */
object ConnectionsDao {

  import ru.tcsbank.system.Global.mySQLTransactionManager._

  val conns = TableQuery[Connections]

  def list = tx { implicit session => conns.list}

  def find(t: String) = tx { implicit session =>
    conns.filter(_.ticket === t).list.headOption
  }

  def insert(c: Connection): Unit = tx { implicit session => conns += c}

  def update(c: Connection) = tx { implicit session =>
    conns.filter(_.ticket === c.ticket).update(c)
  }

  def close(t: String) = find(t) match {
    case Some(c) => update(c.copy(closed = 1))
    case _ => throw new DBAccessException(s"Couldn't close connection with id [$t]")
  }
}
