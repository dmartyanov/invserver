package ru.tcsbank.dao.schema

import ru.tcsbank.dao.model.{User, Users}

import scala.concurrent.Future
import scala.slick.driver.MySQLDriver.simple._

/**
 * Created by d.a.martyanov on 22.12.14.
 */
object UserDao {

  import ru.tcsbank.system.Global.mySQLTransactionManager._

  val users = TableQuery[Users]

  def list = tx { implicit session =>  users.list }

  def sortedList = atx {implicit session => users.list.sortBy(-_.rating) }

  def find(id: String) = tx { implicit session =>
    users.filter(_.userId === id).list.headOption
  }

  def insert(u: User): Unit = tx {implicit session => users += u }

  def append(u: User): Unit = find(u.id) match {
    case Some(c) => ()
    case None => insert(u)
  }
}
