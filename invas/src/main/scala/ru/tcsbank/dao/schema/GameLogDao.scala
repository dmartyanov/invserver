package ru.tcsbank.dao.schema

import ru.tcsbank.dao.model.{GameLog, GameLogs}

import scala.slick.driver.MySQLDriver.simple._

/**
 * Created by d.a.martyanov on 23.12.14.
 */
object GameLogDao {

  import ru.tcsbank.system.Global.mySQLTransactionManager._

  val logs = TableQuery[GameLogs]

  def list = tx {implicit session => logs.list }

  def insert(log: GameLog) = tx { implicit session => logs += log }

  def selectByGameTurn(gameId: String, turn: Int) = atx {implicit session =>
    logs.filter(l => (l.gameId === gameId) && (l.round === turn)).list.headOption
  }
}
