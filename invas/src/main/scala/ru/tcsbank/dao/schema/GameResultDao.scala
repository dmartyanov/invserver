package ru.tcsbank.dao.schema

import ru.tcsbank.dao.model.{GameResult, GameResults}

import scala.slick.driver.MySQLDriver.simple._

/**
 * Created by d.a.martyanov on 23.12.14.
 */
object GameResultDao {

  import ru.tcsbank.system.Global.mySQLTransactionManager._

  val gRes = TableQuery[GameResults]

  def list = tx { implicit session => gRes.list }

  def insert(gr: GameResult) = tx {implicit session => gRes += gr }
}
