package ru.tcsbank.dao.schema

import ru.tcsbank.dao.model.{Game, Games}
import scala.slick.driver.MySQLDriver.simple._

/**
 * Created by d.a.martyanov on 23.12.14.
 */
object GameDao {

  import ru.tcsbank.system.Global.mySQLTransactionManager._

  val games = TableQuery[Games]

  def list = tx {implicit session => games.list }

  def sortedInfo = atx { implicit session =>
    (
      for {
        gs <- games
        grs <- GameResultDao.gRes if gs.gameResultId === grs.resultId
      } yield (gs.gameId, gs.creationDate, grs.winnerId)
    ).sortBy(_._2.desc).list
  }

  def find(gId: String) = tx { implicit session =>
    games.filter(_.gameId === gId).list.headOption
  }

  def afind(gId: String) = atx { implicit session =>
    games.filter(_.gameId === gId).list.headOption
  }

  def insert(g: Game) = tx {implicit session => games += g }

  def update(g: Game) = tx {implicit session =>
    games.filter(_.gameId === g.gameId).update(g)
  }

}
