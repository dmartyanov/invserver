package ru.tcsbank.api

import ru.tcsbank.api.routing.replay.{ReplayModule, ReplayRoute}
import ru.tcsbank.utils.context.ExecutionContextComponent
import spray.routing.HttpService

/**
 * Created by d.a.martyanov on 24.12.14.
 */

trait ReplayWebModule extends ReplayModule with ReplayRoute {
  this: ExecutionContextComponent =>
}

trait ApiHttpService extends HttpService
with ReplayWebModule {
  this: ExecutionContextComponent =>

  val serviceRoute = replayRoute ~ ru.tcsbank.system.Global.staticSprayController.route
}
