package ru.tcsbank.api.engine


/**
 * Created by d.a.martyanov on 24.12.14.
 */
case class Galaxy(
                   planets: List[Planet],
                   players: Map[String, Int] = Map()
                   )

case class BotMove(
                    from: String,
                    to: String,
                    amount: Int,
                    botId: Option[String] = None
                    )

case class Planet(
                   name: String,
                   botAmount: Int,
                   owner: Option[String] = None,
                   neighbors: List[String] = List(),
                   planetType: PlanetType
                   )

sealed class PlanetType(
                         val typeId: Int,
                         val multiplier: Double,
                         val adder: Int,
                         val limit: Int
                         ) {

  def newAmount(owned: Boolean) = (amount: Int) =>
    if (owned) Math.floor(amount * multiplier).toInt + adder
    else amount + adder
}

case object Arzakena extends PlanetType(1, 1.1, 20, 300)

case object Stintino extends PlanetType(2, 1.3, 5, 500)

case object Badesi extends PlanetType(3, 1.6, 10, 800)

case object Cagliari extends PlanetType(4, 2, 20, 2000)


