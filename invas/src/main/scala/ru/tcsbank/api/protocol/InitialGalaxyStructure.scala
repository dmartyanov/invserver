package ru.tcsbank.api.protocol

/**
 * Created by d.a.martyanov on 24.12.14.
 */
case class InitialGalaxyStructure(
                                   gameState: String = "started",
                                   turnNumber: Int = 0,
                                   players: List[PlayerName],
                                   planetMap: List[PlanetElement]
                                   )

case class PlayerName(name: String)

case class PlanetElement(
                          id: String,
                          neighbors: List[String],
                          xCoord: Int,
                          yCoord: Int,
                          pType: String,
                          name: String,
                          owner: String,
                          unitsCount: Int,
                          regenRate: Float = 0.3f
                          )
