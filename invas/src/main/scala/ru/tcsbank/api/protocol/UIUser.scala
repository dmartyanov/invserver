package ru.tcsbank.api.protocol

/**
 * Created by d.a.martyanov on 25.12.14.
 */
case class UIUser(
                   userId: String,
                   rating: Int,
                   gamesNo: Int
                   )
