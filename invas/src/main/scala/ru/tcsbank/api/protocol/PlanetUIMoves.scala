package ru.tcsbank.api.protocol

/**
 * Created by d.a.martyanov on 24.12.14.
 */
case class PlanetUIMoves(
                          turnNumber: Int,
                          gameState: String = "started",
                          playersActions: PlayerActions
                          )

case class PlayerActions(
                          actions: List[BotAction],
                          planetOwners: List[PlanetOwner]
                          )

case class BotAction(
                      owner: String,
                      to: String,
                      from: String,
                      unitCount: Int
                      )

case class PlanetOwner(
                        id: String,
                        unitsCount: Int,
                        owner: String
                        )