package ru.tcsbank.api.protocol

/**
 * Created by d.a.martyanov on 24.12.14.
 */
object PlanetUIInfoRegistry {


  lazy val cordMap14 = Map(
    "h1" -> UIMetaInfo(-100, 325, "TYPE_B", "Heathrhoades"),
    "h2" -> UIMetaInfo(225, 0, "TYPE_B", "Godart"),
    "h3" -> UIMetaInfo(-100, -325, "TYPE_B", "Craigpeters"),
    "h4" -> UIMetaInfo(-425, 0, "TYPE_B", "Horne"),
    "p1" -> UIMetaInfo(-100, 200, "TYPE_A", "Smolders" ),
    "p2" -> UIMetaInfo(41, 141, "TYPE_A", "Izenberg"),
    "p3" -> UIMetaInfo(100, 0, "TYPE_A", "SaintStephan"),
    "p4" -> UIMetaInfo(41, -141, "TYPE_A", "Lautakshing"),
    "p5" -> UIMetaInfo(-100, -200, "TYPE_A", "Briede"),
    "p6" -> UIMetaInfo(-241, -141, "TYPE_A", "Yanney"),
    "p7" -> UIMetaInfo(-300, 0, "TYPE_A", "Poynting"),
    "p8" -> UIMetaInfo(-241, 141, "TYPE_A", "Horne"),
    "c1" -> UIMetaInfo(-90, 35, "TYPE_C", "RedGiant"),
    "c2" -> UIMetaInfo(-110, -35, "TYPE_C", "BlackGiant")
  )
  
  lazy val cordMap25 = Map(
    "h1" -> UIMetaInfo(0, 275, "TYPE_B", "Heathrhoades"),
    "h2" -> UIMetaInfo(275, 0, "TYPE_B", "Godart"),
    "h3" -> UIMetaInfo(0, -275, "TYPE_B", "Craigpeters"),
    "h4" -> UIMetaInfo(-275, 0, "TYPE_B", "Horne"),
    "p1" -> UIMetaInfo(125, 300 , "TYPE_A", "Smolders" ),
    "p2" -> UIMetaInfo(300, 125, "TYPE_A", "Izenberg"),
    "p3" -> UIMetaInfo(300, -125, "TYPE_A", "SaintStephan"),
    "p4" -> UIMetaInfo(125, -300, "TYPE_A", "Lautakshing"),
    "p5" -> UIMetaInfo(-125, -300, "TYPE_A", "Briede"),
    "p6" -> UIMetaInfo(-300, -125, "TYPE_A", "Yanney"),
    "p7" -> UIMetaInfo(-300, 150, "TYPE_A", "Poynting"),
    "p8" -> UIMetaInfo(-125, 300, "TYPE_A", "Horne"),
    "s1" -> UIMetaInfo(175, 175, "TYPE_C", "RedGiant"),
    "s2" -> UIMetaInfo(175, -175, "TYPE_C", "BlackGiant"),
    "s3" -> UIMetaInfo(-175, -175, "TYPE_C", "GreenGiant"),
    "s4" -> UIMetaInfo(-175, 175, "TYPE_C", "BlueGiant"),
    "o1" -> UIMetaInfo(50, 150, "TYPE_A", "Circle1"),
    "o2" -> UIMetaInfo(150, 50, "TYPE_A", "Circle2"),
    "o3" -> UIMetaInfo(150, -50, "TYPE_A", "Circle3"),
    "o4" -> UIMetaInfo(50, -150, "TYPE_A", "Circle4"),
    "o5" -> UIMetaInfo(-50, -150, "TYPE_A", "Circle5"),
    "o6" -> UIMetaInfo(-150, -50, "TYPE_A", "Circle6"),
    "o7" -> UIMetaInfo(-150, 50, "TYPE_A", "Circle7"),
    "o8" -> UIMetaInfo(-50, 150, "TYPE_A", "Circle8"),
    "m1" -> UIMetaInfo(0, 0, "TYPE_D", "BlackHole")
  )

  lazy val cordMap20 = Map(
    "h1" -> UIMetaInfo(100, 300, "TYPE_B", "Heathrhoades"),
    "h2" -> UIMetaInfo(300, 100, "TYPE_B", "Godart"),
    "h3" -> UIMetaInfo(-100, -300, "TYPE_B", "Craigpeters"),
    "h4" -> UIMetaInfo(-300, -100, "TYPE_B", "Horne"),
    "b1" -> UIMetaInfo(150, 150, "TYPE_C", "Smolders"),
    "b2" -> UIMetaInfo(-150, -150, "TYPE_C", "Izenberg"),
    "p1" -> UIMetaInfo(50, 175, "TYPE_A", "SaintStephan"),
    "p2" -> UIMetaInfo(175, 50, "TYPE_A", "Lautakshing"),
    "p3" -> UIMetaInfo(-50, -175, "TYPE_A", "Briede"),
    "p4" -> UIMetaInfo(-175, -50, "TYPE_A", "Poynting"),
    "b3" -> UIMetaInfo(50, 50, "TYPE_C", "RedGiant"),
    "b4" -> UIMetaInfo(-50, -50, "TYPE_C", "RedGiant"),
    "b5" -> UIMetaInfo(-50, 50, "TYPE_C", "RedGiant"),
    "b6" -> UIMetaInfo(50, -50, "TYPE_C", "RedGiant"),
    "p5" -> UIMetaInfo(150, -100, "TYPE_A", "Yanney1"),
    "p6" -> UIMetaInfo(100, -150, "TYPE_A", "Yanney2"),
    "p7" -> UIMetaInfo(-150, 100, "TYPE_A", "Yanney3"),
    "p8" -> UIMetaInfo(-100, 150, "TYPE_A", "Yanney4"),
    "m1" -> UIMetaInfo(200, -200, "TYPE_D", "BlackHole1"),
    "m2" -> UIMetaInfo(-200, 200, "TYPE_D", "BlackHole1")
  )
}

case class UIMetaInfo(xCord: Int, yCord: Int, uiType: String, label: String)
