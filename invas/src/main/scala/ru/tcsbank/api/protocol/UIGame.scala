package ru.tcsbank.api.protocol

/**
 * Created by d.a.martyanov on 26.12.14.
 */
case class UIGame(
                   gameIdent: String,
                   playersList: String,
                   gameWinner: String,
                   gameDate: String
                   )