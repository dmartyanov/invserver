package ru.tcsbank.api.controller

import ru.tcsbank.utils.as.ActorSystemComponent
import spray.routing.Route

/**
 * Created by d.a.martyanov on 24.12.14.
 */
class StaticResourceController(prefix: String, path: String) extends Controller {
  this: ActorSystemComponent =>

  override def route: Route = pathPrefix(prefix){
      getFromDirectory(path)
    }
}
