package ru.tcsbank.api.controller

import ru.tcsbank.api.engine.{BotMove, Galaxy}
import ru.tcsbank.api.protocol._
import ru.tcsbank.dao.schema.{GamePlayersDao, UserDao, GameDao, GameLogDao}
import ru.tcsbank.system.Global.DefaultExecutionContextComponentImpl
import ru.tcsbank.system.{Global, Logger}
import ru.tcsbank.system.exception.{UIGameReplayException, DBAccessException}
import ru.tcsbank.utils.json.JsonHelper
import ru.tcsbank.utils.logging.LoggerHelper

import scala.concurrent.Future
import scala.sys.process.Process

/**
 * Created by d.a.martyanov on 24.12.14.
 */
class GameReplayController {
  this: DefaultExecutionContextComponentImpl with JsonHelper with LoggerHelper =>

  override val lf = Logger[GameReplayController]

  def getUIMap(planetsNo: Int) = planetsNo match {
    case 25 => PlanetUIInfoRegistry.cordMap25
    case 20 => PlanetUIInfoRegistry.cordMap20
    case 14 => PlanetUIInfoRegistry.cordMap14
    case _ => throw new UIGameReplayException("No ui map for such galaxy")
  }

  val neutral = "neutral"

  def loadInitialGalaxyForGame(id: String): Future[InitialGalaxyStructure] = GameDao.afind(id) flatMap {
    case Some(game) if game.initialGalaxy.isDefined =>
      Future {
        parseJS(game.initialGalaxy.get).extract[Galaxy]
      }
    case _ => loggedException(new DBAccessException(s"Game with $id was not found"), ERROR)
  } map { galaxy =>
    val puiMap = getUIMap(galaxy.planets.size)
    InitialGalaxyStructure(
      players = galaxy.players.map { case (pl, bs) => PlayerName(pl)} toList,
      planetMap = galaxy.planets map { pl =>
        PlanetElement(
          id = pl.name,
          neighbors = pl.neighbors,
          xCoord = puiMap(pl.name).xCord,
          yCoord = puiMap(pl.name).yCord,
          pType = puiMap(pl.name).uiType,
          name = puiMap(pl.name).label,
          owner = pl.owner.getOrElse(neutral),
          unitsCount = pl.botAmount
        )
      }
    )
  }

  def loadGameTurnMoves(id: String, turn: Int): Future[PlanetUIMoves] =
    GameLogDao.selectByGameTurn(id, turn) map {
      case Some(log) =>
        val resultGalaxy = parseJS(log.galaxy).extract[Galaxy]
        val moves = parseJS(log.moves).extract[List[BotMove]]
        PlanetUIMoves(
          turnNumber = turn,
          gameState = if(log.last > 0) "finished" else "started",
          playersActions = PlayerActions(
            actions = moves.map(m =>
              BotAction(
                owner = m.botId.getOrElse(neutral),
                from = m.from,
                to = m.to,
                unitCount = m.amount
              )
            ),
            planetOwners = resultGalaxy.planets.map(pl =>
              PlanetOwner(
                id = pl.name,
                unitsCount = pl.botAmount,
                owner = pl.owner.getOrElse(neutral)
              )
            )
          )
        )
      case _ => loggedException(
        new DBAccessException(s"GameLog record with id [$id] and turn [$turn] was not found"), ERROR
      )
    }

  def usersWithRating = UserDao.sortedList map { userDao => userDao map { u =>
    UIUser(u.id, u.rating, GamePlayersDao.playerGames(u.id).size)
  }}

  def allGames = GameDao.sortedInfo map { gameDao => gameDao map { case (gameId, dateLong, winner) =>
    UIGame(
      gameIdent = gameId,
      playersList = GamePlayersDao.gamePlayers(gameId).mkString(", "),
      gameWinner = winner,
      gameDate = new java.util.Date(dateLong).toString
    )
  }}

  def startUniBot: Future[String] = startBot(Global.unibotDir)
  def startRandomBot: Future[String] = startBot(Global.randombotDir)

  private def startBot(dir: String): Future[String] = Future {
    val command: String = s"java -cp $dir/binvbot-assembly-1.0.jar -Dconfig.file=$dir/application.conf -Xms256m -Xmx512m -Djava.awt.headless=true ru.tcsbank.system.Main"
    val pb = Process(command)
    val p = pb.run
    "OK"
  } recover {case err: Exception => s"Error: ${err.getMessage}"}
}
