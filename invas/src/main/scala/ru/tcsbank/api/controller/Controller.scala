package ru.tcsbank.api.controller

import java.net.InetSocketAddress

import akka.actor.ActorRef
import shapeless._
import spray.http.{HttpHeader, HttpHeaders, Rendering}
import spray.routing._

/**
 * Created by d.a.martyanov on 24.12.14.
 */
trait Controller extends Directives {
  def route: Route

  def connectionContext = (headerValueByType[ConnectionContextHeader](()) & headerValueByType[HttpHeaders.`SSL-Session-Info`](())) hmap {
    case connectionContextHeader :: sslInfoHeader :: HNil => connectionContextHeader.context :: sslInfoHeader.info :: HNil
  }
}

case class ConnectionContext(connection: ActorRef, remote: InetSocketAddress, local: InetSocketAddress)

case class ConnectionContextHeader(context: ConnectionContext) extends HttpHeader {
  override def name: String = "X-Connection-Context"

  override def value: String = ""

  override def lowercaseName: String = name.toLowerCase

  override def render[R <: Rendering](r: R): r.type = r
}