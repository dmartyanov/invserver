package ru.tcsbank.api.routing.replay

import ru.tcsbank.api.protocol.{PlayerActions, PlanetUIMoves}
import ru.tcsbank.utils.context.ExecutionContextComponent

import scala.concurrent.Future


/**
 * Created by d.a.martyanov on 24.12.14.
 */
trait ReplayModule {
  this: ExecutionContextComponent =>

  def replayRoutes = Future successful Map("resources" -> List("static/game"))

  def settingsTime = Future successful Map("NextGameTime" -> 1354356000000l)

  def turnDelay = Future successful Map("TurnDelay" -> 2000)

  def initialGalaxy(id : String) = ru.tcsbank.system.Global.replayController.loadInitialGalaxyForGame(id)

  def turnMoves(game: String, turn: String) =
    ru.tcsbank.system.Global.replayController.loadGameTurnMoves(game, turn.toInt)
      .recover {case err: Exception => PlanetUIMoves(turn.toInt, "finished", PlayerActions(List(), List()))}

  def userRatings = ru.tcsbank.system.Global.replayController.usersWithRating

  def allGames = ru.tcsbank.system.Global.replayController.allGames

  def startUniBot = ru.tcsbank.system.Global.replayController.startUniBot
  def startRandomBot = ru.tcsbank.system.Global.replayController.startRandomBot
}
