package ru.tcsbank.api.routing.replay

import org.json4s.{DefaultFormats, Formats}
import ru.tcsbank.utils.context.ExecutionContextComponent
import spray.httpx.Json4sJacksonSupport
import spray.routing.HttpService

/**
 * Created by d.a.martyanov on 24.12.14.
 */
trait ReplayRoute extends HttpService with Json4sJacksonSupport {
  this: ReplayModule with ExecutionContextComponent =>

  override implicit def json4sJacksonFormats: Formats = DefaultFormats

  val replayRoute =
    pathPrefix("replay") {
      (pathEnd & get) {
        onComplete(replayRoutes) { complete(_) }
      } ~ pathPrefix("settings"){
        (path("nextgametime") & get) {
          onComplete(settingsTime) { complete(_) }
        } ~
        (path("turnduration") & get){
          onComplete(turnDelay) { complete(_) }
        }
      } ~ pathPrefix("game"){
        (path("field") & get){
          parameters('gameId){ id =>
            onComplete(initialGalaxy(id)) { complete(_) }
          }
        } ~
        (path("turn") & get){
          parameters('gameId, 'turn){ (gId, t) =>
            onComplete(turnMoves(gId, t)) { complete(_) }
          }
        }
      } ~ (path("ratings") & get){
        onComplete(userRatings){ complete(_) }
      } ~ (path("allgames") & get){
        onComplete(allGames){ complete(_) }
      }
    } ~
      (path("addunibot") & get){
        onComplete(startUniBot){ complete(_)}
      } ~
      (path("addrandombot") & get){
        onComplete(startRandomBot){ complete(_) }
      }
}
