package ru.tcsbank.utils.logging

import com.typesafe.scalalogging.Logger

import scala.util.Failure

/**
 * Created by d.a.martyanov on 24.12.14.
 */
trait LoggerHelper {
  this: LoggerHelper =>

  val lf: Logger

  val ERROR = 1
  val WARN = 2
  val INFO = 3

  def log(msg: String, level: Int) = level match {
    case ERROR => lf.error(msg)
    case WARN => lf.warn(msg)
    case INFO => lf.info(msg)
    case level: Int => lf.debug(s"Error level[$level] - message: [$msg]")
  }

  def logError(err: Throwable, level: Int ) = level match {
    case ERROR => lf.error(err.getMessage, err)
    case WARN => lf.warn(err.getMessage, err)
    case INFO => lf.info(err.getMessage, err)
    case level: Int => lf.debug(s"Error level[$level] - message: [$err.getMessage]", err)
  }

  def logWarn[T](msg: String)(default: => T) = {
    lf.warn(msg)
    default
  }

  def loggedLeft(msg: String, level: Int) = {
    log(msg, level)
    Left(msg)
  }

  def loggedFailure(err: Throwable, level: Int, withStackTrace: Boolean = false) = {
    if(withStackTrace) logError(err, level)
    else log(err.getMessage, level)
    Failure(err)
  }

  def loggedException(err: Throwable, level: Int, withStackTrace: Boolean = false) = {
    if(withStackTrace) logError(err, level)
    else log(err.getMessage, level)
    throw err
  }
}
