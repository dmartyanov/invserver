package ru.tcsbank.utils.as

import akka.actor.ActorSystem

/**
 * Created by d.a.martyanov on 24.12.14.
 */
trait ActorSystemComponent {
  implicit def actorSystem: ActorSystem
}
