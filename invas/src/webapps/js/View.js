/*	TODO list
http://lab.aerotwist.com/webgl/reactive-ball/

 * */

/*


2D and 3D
http://yomotsu.github.com/threejs-examples/box2dwebjs/


asteroids:
http://whiteflashwhitehit.com/content/2011/02/moon_webgl.html



2D coordinate:
http://catchvar.com/threejs-game-transforming-isometric-screen-co

https://github.com/mrdoob/three.js/issues/78


            PERFOMANCE:
            http://learningthreejs.com/blog/2011/09/16/performance-caching-material/    --caching the renderer

 */

var useJSClient = useJSClient || get('useJSClient');

function runPrefixMethod(obj, method) {
    var pfx = ["webkit", "moz", "ms", "o", ""];
    var p = 0, m, t;
    while (p < pfx.length && !obj[m]) {
        m = method;
        if (pfx[p] == "") {
            m = m.substr(0,1).toLowerCase() + m.substr(1);
        }
        m = pfx[p] + m;
        t = typeof obj[m];
        if (t != "undefined") {
            pfx = [pfx[p]];
            return (t == "function" ? obj[m]() : obj[m]);
        }
        p++;
    }
}


THREE.OrbitControlsMod = function ( object, domElement, centers ) {

    THREE.EventTarget.call( this );

    this.object = object;
    this.domElement = ( domElement !== undefined ) ? domElement : document;

    // API

    this.center = centers == undefined ? new THREE.Vector3(): centers[0];

    this.centers = centers == undefined ? [new THREE.Vector3()]: centers;
    //        console.log(this.centers);
    this.currentCenter = 0;

    this.userZoom = true;
    this.userZoomSpeed = 1.0;

    this.userRotate = true;
    this.userRotateSpeed = 1.0;

    this.autoRotate = false;
    this.autoRotateSpeed = 2.0; // 30 seconds per round when fps is 60

    // internals

    var scope = this;

    var EPS = 0.000001;
    var PIXELS_PER_ROUND = 1800;

    var rotateStart = new THREE.Vector2();
    var rotateEnd = new THREE.Vector2();
    var rotateDelta = new THREE.Vector2();

    var zoomStart = new THREE.Vector2();
    var zoomEnd = new THREE.Vector2();
    var zoomDelta = new THREE.Vector2();

    var phiDelta = 0;
    var thetaDelta = 0;
    var scale = 1;

    var lastPosition = new THREE.Vector3();

    var STATE = {
        NONE : -1,
        ROTATE : 0,
        ZOOM : 1,
        PAN : 2
    };
    var state = STATE.NONE;

    // events

    var changeEvent = {
        type: 'change'
    };


    this.rotateLeft = function ( angle ) {

        if ( angle === undefined ) {

            angle = getAutoRotationAngle();

        }

        thetaDelta -= angle;

    };

    this.rotateRight = function ( angle ) {

        if ( angle === undefined ) {

            angle = getAutoRotationAngle();

        }

        thetaDelta += angle;

    };

    this.rotateUp = function ( angle ) {

        if ( angle === undefined ) {

            angle = getAutoRotationAngle();

        }

        phiDelta -= angle;

    };

    this.rotateDown = function ( angle ) {

        if ( angle === undefined ) {

            angle = getAutoRotationAngle();

        }

        phiDelta += angle;

    };

    this.zoomIn = function ( zoomScale ) {

        if ( zoomScale === undefined ) {

            zoomScale = getZoomScale();

        }

        scale /= zoomScale;

    };

    this.zoomOut = function ( zoomScale ) {

        if ( zoomScale === undefined ) {

            zoomScale = getZoomScale();

        }

        scale *= zoomScale;

    };

    this.update = function () {

        var position = this.object.position;
        var offset = position.clone().subSelf( this.center );

        // angle from z-axis around y-axis

        var theta = Math.atan2( offset.x, offset.z );

        // angle from y-axis

        var phi = Math.atan2( Math.sqrt( offset.x * offset.x + offset.z * offset.z ), offset.y );

        if ( this.autoRotate ) {

            this.rotateLeft( getAutoRotationAngle() );

        }

        theta += thetaDelta;
        phi += phiDelta;

        // restrict phi to be betwee EPS and PI-EPS

        phi = Math.max( EPS, Math.min( Math.PI - EPS, phi ) );

        var radius = offset.length();
        offset.x = radius * Math.sin( phi ) * Math.sin( theta );
        offset.y = radius * Math.cos( phi );
        offset.z = radius * Math.sin( phi ) * Math.cos( theta );
        offset.multiplyScalar( scale );

        position.copy( this.center ).addSelf( offset );

        this.object.lookAt( this.center );

        thetaDelta = 0;
        phiDelta = 0;
        scale = 1;

        if ( lastPosition.distanceTo( this.object.position ) > 0 ) {

            this.dispatchEvent( changeEvent );

            lastPosition.copy( this.object.position );

        }

    };


    function getAutoRotationAngle() {

        return 2 * Math.PI / 60 / 60 * scope.autoRotateSpeed;

    }

    function getZoomScale() {

        return Math.pow( 2.95, scope.userZoomSpeed );

    }

    function onMouseDown( event ) {

        if ( !scope.userRotate ) return;

        event.preventDefault();

        if ( event.button === 0 ) {

            state = STATE.ROTATE;

            rotateStart.set( event.clientX, event.clientY );

        }
        else
        if ( event.button === 2 ) {
            if(scope.currentCenter < scope.centers.length-1) {
                scope.currentCenter++;
            } else {
                scope.currentCenter = 0;
            }

            scope.center = scope.centers[scope.currentCenter];

        }

        document.addEventListener( 'mousemove', onMouseMove, false );
        document.addEventListener( 'mouseup', onMouseUp, false );

    }

    function onMouseMove( event ) {

        event.preventDefault();

        if ( state === STATE.ROTATE ) {

            rotateEnd.set( event.clientX, event.clientY );
            rotateDelta.sub( rotateEnd, rotateStart );

            scope.rotateLeft( 2 * Math.PI * rotateDelta.x / PIXELS_PER_ROUND * scope.userRotateSpeed );
            scope.rotateUp( 2 * Math.PI * rotateDelta.y / PIXELS_PER_ROUND * scope.userRotateSpeed );

            rotateStart.copy( rotateEnd );

        } else if ( state === STATE.ZOOM ) {

            zoomEnd.set( event.clientX, event.clientY );
            zoomDelta.sub( zoomEnd, zoomStart );

            if ( zoomDelta.y > 0 ) {
                scope.zoomOut();

            } else {

                scope.zoomIn();

            }

            zoomStart.copy( zoomEnd );

        }

    }

    function onMouseUp( event ) {

        if ( ! scope.userRotate ) return;

        document.removeEventListener( 'mousemove', onMouseMove, false );
        document.removeEventListener( 'mouseup', onMouseUp, false );

        state = STATE.NONE;

    }

    function onMouseWheel( event ) {
        event.preventDefault();
        if ( ! scope.userZoom ) return;

        if ( event.wheelDelta > 0 || event.detail > 0) {

            scope.zoomOut();

        } else {

            scope.zoomIn();

        }

    }

    this.domElement.addEventListener( 'contextmenu', function ( event ) {
        event.preventDefault();
    }, false );
    this.domElement.addEventListener( 'mousedown', onMouseDown, false );
    this.domElement.addEventListener( 'mousewheel', onMouseWheel, false );
    this.domElement.addEventListener('DOMMouseScroll', onMouseWheel, false);

    window.ChromeWheel  = function() {
        var evt = document.createEvent("MouseEvents");
        evt.initMouseEvent(
            'DOMMouseScroll', // in DOMString typeArg,
            true,  // in boolean canBubbleArg,
            true,  // in boolean cancelableArg,
            window,// in views::AbstractView viewArg,
            120,   // in long detailArg,
            0,     // in long screenXArg,
            0,     // in long screenYArg,
            0,     // in long clientXArg,
            0,     // in long clientYArg,
            0,     // in boolean ctrlKeyArg,
            0,     // in boolean altKeyArg,
            0,     // in boolean shiftKeyArg,
            0,     // in boolean metaKeyArg,
            0,     // in unsigned short buttonArg,
            null   // in EventTarget relatedTargetArg
            );
        this.domElement.dispatchEvent(evt);
    }
};

var renderType = 'WebGL';


var shaderBlock = {
    sunUniforms : null,

    sunFShader : [
    "uniform float time;",
    "uniform vec2 resolution;",
    "uniform float fogDensity;",
    "uniform vec3 fogColor;	",
    "uniform sampler2D texture1;",
    "uniform sampler2D texture2;",

    "varying vec2 vUv;",

    "void main( void ) {",
    "vec2 position = -1.0 + 2.0 * vUv;",
    "vec4 noise = texture2D( texture1, vUv );",

    "vec2 T1 = vUv + vec2( 1.5, -1.5 ) * time  *0.02;",
    "vec2 T2 = vUv + vec2( -0.5, 2.0 ) * time * 0.01;",

    "T1.x += noise.x * 2.0;",
    "T1.y += noise.y * 2.0;",
    "T2.x -= noise.y * 0.2;",
    "T2.y += noise.z * 0.2;",

    "float p = texture2D( texture1, T1 * 2.0 ).a;",
    "vec4 color = texture2D( texture2, T2 * 2.0 );",
    "vec4 temp = color * ( vec4( p, p, p, p ) * 2.0 ) + ( color * color - 0.1 );",

    "if( temp.r > 1.0 ){ temp.bg += clamp( temp.r - 2.0, 0.0, 100.0 ); }",
    "if( temp.g > 1.0 ){ temp.rb += temp.g - 1.0; }	",
    "if( temp.b > 1.0 ){ temp.rg += temp.b - 1.0; }	",

    "gl_FragColor = temp;",
    "float depth = gl_FragCoord.z / gl_FragCoord.w;",
    "const float LOG2 = 1.442695;",
    "float fogFactor = exp2( - fogDensity * fogDensity * depth * depth * LOG2 );",
    "fogFactor = 1.0 - clamp( fogFactor, 0.0, 1.0 );",
    "gl_FragColor = mix( gl_FragColor, vec4( fogColor, gl_FragColor.w ), fogFactor );}"
    ].join("\n") ,

    sunVShader : [
    "uniform vec2 uvScale;",

    "varying vec2 vUv;",

    "void main()	{",
    "vUv = uvScale * uv;",
    "vec4 mvPosition = modelViewMatrix * vec4( position, 0.9 );",
    "gl_Position = projectionMatrix * mvPosition;	",
    "}"
    ].join("\n") ,


    material: null,

    loadSun : function (size) {

        uniforms = {
            fogDensity: {
                type: "f",
                value: 0.00001
            },
            fogColor: {
                type: "v3",
                value: new THREE.Vector3( 0,0, 0 )
            },
            time: {
                type: "f",
                value: 1.0
            },
            resolution: {
                type: "v2",
                value: new THREE.Vector2()
            },
            uvScale: {
                type: "v2",
                value: new THREE.Vector2( 3.0, 1.0 )
            },
            texture1: {
                type: "t",
                value: 0,
                texture: THREE.ImageUtils.loadTexture( './08.jpg')
            //                texture: THREE.ImageUtils.loadTexture( './js/lavatile.jpg')
            },
            texture2: {
                type: "t",
                value: 1,
                texture: THREE.ImageUtils.loadTexture( './08.jpg')
            //                texture: THREE.ImageUtils.loadTexture( './js/lavatile.jpg')
            }
        }

        uniforms.texture1.texture.wrapS = uniforms.texture1.texture.wrapT = THREE.RepeatWrapping;

        uniforms.texture2.texture.wrapS = uniforms.texture2.texture.wrapT = THREE.RepeatWrapping;
        shaderBlock.sunUniforms = uniforms;

        //        var size = renderBlock.getPlanetSize('TYPE_A');

        shaderBlock.material = new THREE.ShaderMaterial( {

            uniforms: shaderBlock.sunUniforms,
            vertexShader: shaderBlock.sunVShader,
            fragmentShader: shaderBlock.sunFShader

        } );
        //shaderBlock.material.transparent = true;
        //shaderBlock.material.opacity = 0.9;
        shaderBlock.material.color=0x222222;
        // sphere
        var sun = new THREE.Mesh(

            new THREE.SphereGeometry(
                size,
                renderBlock.config.lod*2,
                renderBlock.config.lod),

            shaderBlock.material
            );

        //        sun.overdraw = true;

        //        console.log(sun);
        renderBlock.sun = sun;
        return sun;

    }

};



var modelBlock = {

    initiated: false,
    players: {},
    planetMap: {},
    actionMap: {},
    prevStep: {},
    controllers:{},
    jsonResponse: [],
    stats  : {

    },
    max: 0,
    turn: 0,
    statGui : null,


    initStatGui : function() {

        modelBlock.statGui = new DAT.GUI({
            autoPlace: false,
            height	: modelBlock.players.length * 32 - 1
        });

        var customContainer = document.body.appendChild(document.createElement('div'));
        customContainer.id = 'stats';
        customContainer.appendChild(modelBlock.statGui.domElement);

        var style = customContainer.style;
        style.visibility = "visible";
        style.position = "fixed";
        style.top = '0px';
        style.left = '0px';
        customContainer.style = style;
        for(player in modelBlock.players ){
            name = modelBlock.players[player].name;
            modelBlock.stats[name] = 0;
            var controller =  modelBlock.statGui.add(modelBlock.stats, name,0);
            controller.listen();
            controller.min(0);
            controller.max(0);
            modelBlock.controllers[name] = controller;
        }
    },
    initStats: function() {
        for(player in modelBlock.players ){
            name = modelBlock.players[player].name;
            modelBlock.stats[name] = 0;
        }
    },

    updateStats: function() {
        for(player in modelBlock.players) {
            name = modelBlock.players[player].name;
            modelBlock.stats[name] = 0;
        }
        map = modelBlock.planetMap;

        for (planetnum in map) {
            planet = map[planetnum];
            if(planet.owner !='neutral') {
                modelBlock.stats[planet.owner] +=planet.unitsCount;
            }
        }
        var max = 0;
        var leader = '';
        for(stat in modelBlock.stats) {
            if(modelBlock.stats[stat] > max) {
                max = modelBlock.stats[stat];
                leader = stat;
            }
        }
        modelBlock.max = max;
        modelBlock.leader = leader;
        var leaders = [];
        for(stat in modelBlock.stats) {
            if(modelBlock.stats[stat] == max) {
                leaders.push(stat);
            }
        }
        modelBlock.leaders = leaders;

    //        console.log(modelBlock.leader);
    //        if(renderBlock.statCanvas != null)renderBlock.programStats(renderBlock.statCanvas.getContext("2d"));

    //        for(c in modelBlock.controllers) {
    //            modelBlock.controllers[c].max(max);
    //        }
    },




    vars: {
        container: null,
        playerCount: null
    },

    init: function(json) {
        //        console.log("entered the modelBLock.init() with json:");
        //        console.log(json);
        if (!modelBlock.initiated) {

            //            var divs = document.getElementsByClassName('gameStatusMessage');
            //            for (var i = 0; i < divs.length; i++) {
            //                divs[i].style.display = "none";
            //            }

            if (json.planetMap.length > 0) {

                modelBlock.planetMap = json.planetMap;
                modelBlock.turn = get('turn') ? parseInt( get('turn') ): json.turnNumber;
                modelBlock.jsonResponse.push(json);
                modelBlock.prevStep[modelBlock.turn] = modelBlock.planetMap.slice();
                modelBlock.players = json.players;

                modelBlock.vars.playerCount = modelBlock.players.length;
                modelBlock.initiated = true;
                
                // next string
                //modelBlock.initStatGui();
                modelBlock.initStats();

            } else {
                console.log('empty response');
            }
        }
    }
};
/////////////////////////////////////////

///// LOOP BLOCK ///////////////////////////////////////////////////////////////////////////////////////////////////////////
var loopBlock = {

    //TODO get the coordinates correction, caused by fullscreen and div placement
    onDocumentMouseMove : function(event) {
        //TODO add support for the non-fullscreen mode
        var vector;
        if(renderBlock.GUIOptions.fullScreen ) {
            vector = new THREE.Vector3( ( event.clientX / renderBlock.SCREEN_WIDTH ) * 2 - 1, - ( event.clientY / renderBlock.SCREEN_HEIGHT ) * 2 + 1, 0.5 );
        } else {

            _this = $('#container');
            vector = new THREE.Vector3(
                ( (event.clientX - (_this.offset().left - $(window).scrollLeft()) ) / renderBlock.SCREEN_WIDTH ) * 2 - 1,
                -( (event.clientY - (_this.offset().top - $(window).scrollTop()) ) / renderBlock.SCREEN_HEIGHT) * 2 + 1,
                0.5
                );
        //           console.log(_this.offset().top - $(window).scrollTop()) ;
        }
        //         console.log(vector.x + ' ' + vector.y + ' ' + vector.z);
        renderBlock.projector.unprojectVector( vector, renderBlock.camera );
        INTERSECTED = renderBlock.INTERSECTED;
        var ray = new THREE.Ray( renderBlock.camera.position, vector.subSelf(renderBlock.camera.position ).normalize() );

        var intersects = ray.intersectObjects( renderBlock.mesh.children );

        if ( intersects.length > 0 ) {

            if ( INTERSECTED != intersects[ 0 ].object ) {

                if(renderType == 'WebGL'){

                    if ( INTERSECTED ) {
                        INTERSECTED.children[1].visible = !renderBlock.GUIOptions.hideWire;
                        INTERSECTED.children[1].scale.set(1.0, 1.0, 1.0);
                    }
                    intersects[ 0 ].object.children[1].visible = true;
                    intersects[ 0 ].object.children[1].scale.set(1.2, 1.2, 1.2);
                    INTERSECTED = intersects[ 0 ].object;

                } else {

                    if ( INTERSECTED ) INTERSECTED.material.program = renderBlock.programStroke;
                    INTERSECTED = intersects[ 0 ].object;
                    INTERSECTED.material.program = renderBlock.programFill;
                }

                id = intersects[ 0 ].object.planetId;
                for(arrow in renderBlock.actionMap) {
                    if(arrow.split(' ')[0] == id){
                        renderBlock.actionMap[arrow].line.visible = true;
                    }
                }

            }

        } else if(INTERSECTED){
            if(renderType == 'WebGL'){
                INTERSECTED.children[1].visible = !renderBlock.GUIOptions.hideWire;
                INTERSECTED.children[1].scale.set(1.0, 1.0, 1.0);
            } else {
                INTERSECTED.material.program = renderBlock.programStroke;
            }
            id = INTERSECTED.planetId;
            for(arrow in renderBlock.actionMap) {
                if(arrow.split(' ')[0] == id){
                    renderBlock.actionMap[arrow].line.visible = INTERSECTED.triggered;
                }
            }
            INTERSECTED = null;

        }
        renderBlock.INTERSECTED = INTERSECTED;

    },

    actionsFinished : false,
    jsonHandler: function(json, xmlhttp) {
        //        console.log('\n\nprevTurn: ' + modelBlock.turn + ' newTurn: ' + json.turnNumber );
    	modelBlock.turn = json.turnNumber;
    	
    	// update
    	setTurn( modelBlock.turn );
    	
        //        modelBlock.prevStep[modelBlock.turn] = modelBlock.planetMap.slice();

        //                        modelBlock.jsonResponse.push(json);
        planetOwners = json.playersActions.planetOwners;
        for(p in planetOwners) {

            for(mp in modelBlock.planetMap)  {
                if(planetOwners[p].id == modelBlock.planetMap[mp].id) {
                    modelBlock.planetMap[mp].owner = planetOwners[p].owner;
                    modelBlock.planetMap[mp].unitsCount = planetOwners[p].unitsCount;
                }
            }

        }
        modelBlock.actionMap = json.playersActions.actions;

        //        console.log('new turn:' + modelBlock.turn );

        res = renderBlock.updateView();

        if(!res) {
            console.log(xmlhttp.responseText);
        }
        modelBlock.updateStats();

    },

    updateActions: function ( ){
    	
        var mapId = gameId;
    	ajaxRequest( 
    			"../replay/game/turn?gameId=" + mapId + "&turn=" + (modelBlock.turn + 1),
    			function( xmlhttp )
    			{    				
    				try {
                        json = JSON.parse(xmlhttp.responseText);
                    } catch(e) {
                        window.clearInterval(initBlock.actionUpdater);
                    }
                    
                    switch(json.gameState) {
                        case 'started':{
                            if(modelBlock.turn != json.turnNumber){
                                loopBlock.jsonHandler(json, xmlhttp);
                                modelBlock.jsonResponse.push(json);
                            }                            
                            break;
                        }
                        case 'finished': {
                            if(!loopBlock.actionsFinished) {
                                //                            if(modelBlock.turn != json.turnNumber){
                                loopBlock.jsonHandler(json, xmlhttp);
                                window.clearInterval(initBlock.actionUpdater);
                                loopBlock.updateActions();
                                modelBlock.turn = json.turnNumber;                                
                                loopBlock.actionsFinished = true;
                                console.log('ending');
                            //                            }

                            } else {
                                console.log('ended');
                                window.clearInterval(initBlock.actionUpdater);

                                initBlock.notGameStatus(json);
                            }
                            break;
                        }
                        default:
                            initBlock.notGameStatus(json);
                            
                            break;

                    }
    			});
/*        
        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP ");
        }

        var request = "game/viewData.html?gameId=" + mapId + "&type=PLAYERS_ACTIONS";
        xmlhttp.open("GET", request, true);
        xmlhttp.send();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                try {
                    json = JSON.parse(xmlhttp.responseText);
                } catch(e) {
                    window.clearInterval(initBlock.actionUpdater);
                }
                
                switch(json.gameState) {
                    case 'started':{
                        if(modelBlock.turn != json.turnNumber){
                            loopBlock.jsonHandler(json, xmlhttp);
                            modelBlock.jsonResponse.push(json);
                        }
                        break;
                    }
                    case 'finished': {
                        console.log('hrum hrum');
                        if(!loopBlock.actionsFinished) {
                            console.log('yup yup');
                            //                            if(modelBlock.turn != json.turnNumber){
                            console.log('om nom nom');
                            loopBlock.jsonHandler(json, xmlhttp);
                            window.clearInterval(initBlock.actionUpdater);
                            loopBlock.updateActions();
                            modelBlock.turn = json.turnNumber;
                            loopBlock.actionsFinished = true;
                            console.log('ending');
                        //                            }

                        } else {
                            console.log('ended');
                            window.clearInterval(initBlock.actionUpdater);

                            initBlock.notGameStatus(json);
                        }
                        break;
                    }
                    default:
                        initBlock.notGameStatus(json);
                        
                        break;

                }
            }
        };
*/
    }
};
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function  animate() {

    window.requestAnimationFrame(animate, renderBlock.renderer.domElement);

    renderBlock.render();
}

function chooseRenderType() {
	
	ajaxRequest(
			"../replay/settings/turnduration",
			function( xmlhttp )
			{
				renderBlock.turnSpeed = JSON.parse(xmlhttp.responseText).TurnDelay;
            	console.log(xmlhttp.responseText);
			});
	
    //    console.log('a');

    //    if (!Detector.webgl) {
    //        //            Detector.addGetWebGLMessage();
    //        window.renderType='Canvas';
    //        console.log(' canvas render');
    //        initBlock.getJSONModel();
    //
    //    } else {
    //        window.renderType='WebGL';
    //        console.log(' GL render');
    //        initBlock.getJSONModel();
    //    }

    window.renderType='Canvas';
    console.log('chosed the canvas render(or try "WebGL" if wants more)');
    
    if( useJSClient )
    	return;
    	
    if( get('gid') )
    {
    	setGID( get('gid') ) 
    	initBlock.getJSONModel();
    }
    else
    {
		ajaxRequest(
				"./bot?type=CREATE_GAME",
				function( xmlhttp )
				{
					console.log(xmlhttp.responseText);
					setGID( JSON.parse(xmlhttp.responseText) );	
					
					// recall
					initBlock.getJSONModel();					
				});
    }
}

function setTurn( turn )
{
	if( $('#turnId').size() == 0 )
		$('body').prepend('<div id=turnId></div>');

	$('#turnId').text( turn );
}

function setGID( gameId )
{
	if( $('#gameId').size() == 0 )
		$('body').prepend('<div id=gameId></div>');

	$('#gameId').text( gameId );
}

function onDocumentKeyDown( event ) {
    
    switch( event.keyCode ) {

        case 13: {
            
            break;
        }
        case 38: {//up
            console.log(renderBlock.camera.position);
            break;
        }
        case 40 :{
            
            break;
        }
        case 9 : {
            
            break;
        }
        case 70: {//f - fullscreen
            console.log('f was pressed');
            if(renderBlock.started) {
                renderBlock.GUIOptions.fullScreen = ! renderBlock.GUIOptions.fullScreen;
                renderBlock.turnOnFullScreen();
            }
            break;
        }
        case 83: { //s - show trace
            renderBlock.GUIOptions.showSelection = !renderBlock.GUIOptions.showSelection;
            for(planet in renderBlock.planets) {
                renderBlock.planets[planet].triggered = renderBlock.GUIOptions.showSelection;
            }
            for(arrow in renderBlock.actionMap){
                renderBlock.actionMap[arrow].line.visible = renderBlock.GUIOptions.showSelection;
            }
        }
        default: {
            //            console.log(event.keyCode);
            break;
        }
    }
}

document.addEventListener( 'keydown', onDocumentKeyDown, false );

document.onmozfullscreenchange = document.onwebkitfullscreenchange = function() {
    if(!THREEx.FullScreen.activated() ) {
        THREEx.FullScreen.cancel();
        $('canvas').css({
            "background-image":"none"
        });
        try {
            leaveGame = document.getElementById("leaveGame");
            leaveGame.style.visibility = "visible";
        } catch(e){}
        divs = document.getElementsByClassName('wrap');
        for ( i = 0; i < divs.length; i++) {
            divs[i].style.visibility = "visible";
        }
        renderBlock.renderer.domElement.style.top = '0px';
        renderBlock.SCREEN_WIDTH = $('#container').outerWidth();
        renderBlock.SCREEN_HEIGHT = $('#container').outerHeight();
        renderBlock.renderer.setSize(renderBlock.SCREEN_WIDTH, renderBlock.SCREEN_HEIGHT);
        renderBlock.renderer.domElement.style.position = "relative";

        renderBlock.camera.aspect = renderBlock.SCREEN_WIDTH / renderBlock.SCREEN_HEIGHT;
        renderBlock.camera.updateProjectionMatrix();
        if(renderType=='WebGL') {
            renderBlock.effectFXAA.uniforms[ 'resolution' ].value.set( 1 / window.innerWidth, 1 / window.innerHeight );
            renderBlock.initComposer(renderBlock.enableFXAA);
        }
        renderBlock.GUIOptions.fullScreen = false;
    }
};
window.onload = chooseRenderType;
