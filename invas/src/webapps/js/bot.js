var bot = {
	automated: false,
	gameId: null,
	fieldId: null,
	uid: null,
	logField: null,
	waitOnCreate: 100000,
	
	register : function( name )
	{
		$.ajax({
			url: "bot?name=" + name + "&type=REGISTER",
			async: false,
			type: "GET",
			success: function( responseText )
			{
	 			bot.uid = responseText;
	 			bot.logField.append( "OK<br>" + responseText  + "<BR>");
			}
		});
	},

	createGame : function( fieldId, logic )
	{
		$.ajax({
			url: "bot?gameField=" + fieldId + 
				"&uid=" + bot.uid + 
				"&logic="+ logic +
				"&waitOnCreate=" + bot.waitOnCreate +
				"&type=CREATE_GAME",
			async: false,
			type: "GET",
			success: function( responseText )
			{
				bot.gameId = responseText;
				bot.logField.append( "OK<br>" + responseText + "<BR>");
			}
		});
	},

	selectGame : function( gameId )
	{
		$.ajax({
			url: "bot?gameId=" + (gameId ? gameId : bot.gameId) + 
				"&uid=" + bot.uid + 
				"&type=SELECT_GAME",
			async: false,
			type: "GET",
			success: function( responseText )
			{
				$('#gameId').remove();
				
				bot.gameId = responseText;
				bot.logField.append( "OK<br>" + responseText  + "<BR>");
			}
		});
	},

	wait : function()
	{		
		$.ajax({
			url: "bot?gameId=" + bot.gameId + "&uid=" + bot.uid + "&type=WAIT", 
			async: false,
			type: "GET",
			success: function( responseText )
			{
				bot.logField.append( responseText  + "<BR>");
				
				if( responseText != "OK")
				{
					setTimeout( bot.wait, 1000 );
				}
				else if( !bot.automated )
				{
					if( $('#gameId').size() == 0 )
					{
						$('#notify').text('Игра началась');
						
						console.log( "Set gid: " + bot.gameId );
						setGID( bot.gameId );
						initBlock.getJSONModel();
					}
					else
					{
						$('#notify').text('Следущий раунд');
						
						loopBlock.updateActions();
					}
				}
			}
		});
	},	
	
	startRound : function()
	{
		$.ajax({
			url: "bot?uid=" + bot.uid + "&type=GET_ROUND&result=JSON",
			async: false,
			type: "GET",
			success: function( responseText )
			{
				bot.logField.append( JSON.stringify( responseText, null, "\t") + "<BR>");
				$('#notify').text('Начался раунд');					
			}
		});
	},
	
	makeMoves : function( moves )
	{
		$.ajax({
			url: "bot",
			data: 
			{ 
				uid: bot.uid, 
				movesJSON: JSON.stringify(moves),
				type: "MAKE_MOVE" 
			},
			async: false,
			type: "POST",
			success: function( responseText )
			{
				bot.logField.append( "OK<br>" + responseText  + "<BR>");
				
				if( responseText == "OK" )
					bot.wait();
			}
		});
	},
/*	
	ajaxRequest : function( URL, lamda, priority, timeout )
	{
		var newOne = ( requestFifo.length == 0 );
		var newLamda = lamda;
		var newUrl = URL;
		var newTimeout = timeout
		var processFunction = function() {
				ajaxRequest( newUrl, function( responseText )
					{				
						try
						{
							newLamda( responseText );
						}
						catch( err )
						{
							console.log( err );
						}

						// remove executed one
						requestFifo.shift();
						
						// pop and execute
						if( requestFifo.length > 0 )
						{
							setTimeout( requestFifo[0](), newTimeout);
						}
					});
			};
		
		if( priority )
		{			
			requestFifo.unshift( processFunction );
		}
		else
		{
			requestFifo.push( processFunction );
		}
		
		if( newOne )
		{
			requestFifo[0]();
		}
	},
*/	
	sequental: function( lamda, priority  )
	{
		console.log("add " + lamda );
		
		var newOne = requestFifo.length == 0;
		
		if( priority )
			requestFifo.unshift( lamda );
		else
			requestFifo.push( lamda );
		
		if( newOne )
		{
			console.log("execute first one " + lamda );
			requestFifo[0]();
		}
	},
		
	ajaxRequest : function( URL, lamda, timeout )
	{
		ajaxRequest( URL, function( responseText )
			{						
				try
				{
					lamda( responseText );
				}
				catch( err )
				{
					console.log( err );
				}

				// remove executed one
				requestFifo.shift();
								
				console.log("remove " + lamda );
				
				// pop and execute
				if( requestFifo.length > 0 )
				{
					// execute next
					console.log("execute next " + requestFifo[0] + " t:" + timeout );
					setTimeout( requestFifo[0](), timeout ? timeout : 100);
				}
			});
	}
	
};

var inRequest = false;
var requestFifo = new Array();
