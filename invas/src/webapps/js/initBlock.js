var useJSClient = useJSClient || get('useJSClient'); 
///// INIT BLOCK ///////////////////////////////////////////////////////////////////////////////////////////////////////////
var initBlock = {
    modelUpdater: null,
    actionUpdater:null,
    jsonGetter: null,
    useJSClient: useJSClient,
    config: {
        gameId: null,
        sunAsObject: true
    },

    init: function (json) {
        window.clearInterval(initBlock.jsonGetter);
        document.body.appendChild(document.createElement('div')).id = 'border';
        modelBlock.init(json);
        //        modelBlock.planetMap = json.planetMap;
        modelBlock.updateStats();
        renderBlock.init();
        
        if( !useJSClient )
        {
        	loopBlock.updateActions();
        
        	window.addEventListener('resize', renderBlock.onWindowResize, false);
        	animate();
        	
        	initBlock.actionUpdater = window.setInterval(loopBlock.updateActions, renderBlock.turnSpeed/2);
        }
        else
        {
        	window.addEventListener('resize', renderBlock.onWindowResize, false);
        	animate();        	
        }
    },

    showWinners: function(scores) {

        span = document.getElementById("winner");
        span.innerHTML = "Победители:&nbsp";
        span.style.display = "none";
        span = document.getElementById("winnerName");
        var table = "<table id='winnerTable' width = '100%'> \n\
         <tr>\n\
            <th>Игрок</th>\n\
            <th>Последний ход</th>\n\
            <th>Количество дроидов</th>\n\
            <th>Место</th>\n\
        </tr>"

        for(var i = 0; i < scores.length; i++) {
            table +="<tr><td>" + scores[i].player + "</td>";
            table +="<td>" + scores[i].lastTurn + "</td>";
            table +="<td>" + scores[i].unitsCount + "</td>";
            table+="<td>" + scores[i].place + "</td></tr>";
        }
        table +="</table>";

        span.innerHTML = table;
        span.style.fontSize = '24px';
        span.style.display = "block";
        //        span.style.marginTop = '20px';
        span.style.backgroundColor = 'rgba(42,42,42,0.7)';

        

    },

    notGameStatus: function (message) {
        if (message.gameState == "finished") {
            
            console.log('finished');
            divs = document.getElementsByClassName('gameStatusMessage');
            for (var i = 0; i < divs.length; i++) {
                divs[i].style.display = "block";
            }
            span = document.getElementById("notStarted");
            span.style.display = "none";
            //            span.style.visibility = 'hidden';
            
            span = document.getElementById("finished");
            span.style.display = "block ";
            span.style.paddingTop = '20px';

            if(renderBlock.started) {
                initBlock.showWinners(message.scores);
            }
            if(!renderBlock.started) {
                $('#container').css('height', '0px');
            }

        } else { //not started
            if(renderBlock.started==false) {
                if(!initBlock.modelUpdater)
                    initBlock.modelUpdater = window.setInterval(initBlock.getJSONModel, renderBlock.turnSpeed/2);
            }
            divs = document.getElementsByClassName('gameStatusMessage');
            for (var i = 0; i < divs.length; i++) {
                divs[i].style.display = "block ";
            }
            span = document.getElementById("notStarted");
            span.style.display = "block ";
            //            span.style.visibility = 'visible';
            span = document.getElementById("finished");
            span.style.display = "none ";
        //            span.style.visibility = 'hidden';

        }
    },

    hideWarnings: function() {
        //        divs = document.getElementsByClassName('gameStatusMessage');
        //        for (var i = 0; i < divs.length; i++) {
        //            divs[i].style.display = "none";
        //        }
        span = document.getElementById("notStarted");
        //        span.style.visibility = 'hidden';
        span.style.display = "none ";
        span = document.getElementById("finished");
        //        span.style.visibility = 'hidden';
        span.style.display = "none ";
    },
    started : false,

    getJSONModel: function () 
    {
        gameId = $('#gameId').text();
        
        if(gameId) {
            var mapId = gameId;
        
            ajaxRequest(
            	"../replay/game/field?gameId=" + mapId,
            	function( xmlhttp )
            	{
                    json = JSON.parse(xmlhttp.responseText);
                    
                    switch(json.gameState) {
                        case 'started':
                            if(!initBlock.started) {
                                try {
                                    window.clearInterval(initBlock.modelUpdater);
                                } catch(e) {}
                                initBlock.hideWarnings();
                                initBlock.started = true;
                                initBlock.init(json);
                            }
                            break;
                        default:
                            initBlock.notGameStatus(json);
                        
                            break;

                    }
            	});            
        } else {
        	;//
        }
    }
};
