
var GlobalTimer = Class.create(
{
	initialize        : function(pause)
	{
		if(!pause) pause = 250;
		this._pause = pause;
		this._ops   = [];

		this._timer_bind = this._timer.bind(this);
		setInterval(this._timer_bind, pause)
	},

	on                : function(op)
	{
		if(!this._ops.include(op))
			this._ops.push(op)
	},

	off               : function(op)
	{
		var i = this._ops.indexOf(op);
		if(i != -1) this._ops.splice(i, 1)
	},

	_timer            : function()
	{
		for(var i = 0;(i < this._ops.length);i++)
			this._ops[i] && this._ops[i]()
	}
})

window.globalTimer = window.globalTimer || new GlobalTimer(100);


var FadeEffect = Class.create({

	initialize        : function(opts)
	{
		this._opts = {
		  timer: window.globalTimer,
		  turns: 40,
		  hide: true
		}

		Object.extend(this._opts, opts)
		this._timer_bind = this._timer.bind(this);

		this._items = [];
		if(opts.item)
			this._items.push(opts.item)
		if(opts.items)
			this._items = [this._items, opts.items].flatten()
	},

	start             : function()
	{
		if(this._started) return;
		this._started = true;

		this._turn = 0;
		this._display()
		this._opts.timer.on(this._timer_bind)
	},

	finish            : function()
	{
		if(!this._started) return;

		this._free()
		this._hide()
	},

	_timer            : function()
	{
		if(this._turn++ > this._opts.turns)
		{
			this.finish()
			return;
		}

		this._display()
		this._turn++;
	},

	_display          : function()
	{
		//HINT: turn in [0; turns]

		if(this._turn == 0)
			this._show()
		else if(this._turn <= this._opts.turns)
			this._fade()
	},

	_fade             : function()
	{
		for(var i = 0;(i < this._items.length);i++)
			this._fade_node($(this._items[i]))
	},

	_fade_node        : function(node)
	{
		if(!node) return;
		node.setOpacity(1.0 - this._turn/this._opts.turns)
	},

	_show             : function()
	{
		for(var i = 0;(i < this._items.length);i++)
			this._show_node($(this._items[i]))
	},

	_show_node        : function(node)
	{
		if(!node) return;

		node.show()
		node.setStyle({visibility: 'visible'})
		node.setOpacity(1)
	},

	_hide             : function()
	{
		for(var i = 0;(i < this._items.length);i++)
			this._hide_node($(this._items[i]))
	},

	_hide_node        : function(node)
	{
		if(!node) return;

		if(this._opts.hide) node.hide()
		else node.setStyle({visibility: 'hidden'})
		node.setOpacity(1)
	},

	_free             : function()
	{
		this._opts.timer.off(this._timer_bind)
		this._started = false;
	}
})