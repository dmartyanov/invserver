function getMap(mapId)
{	
	ajaxRequest( 
		"game/viewData?gameid=" + mapId + "&type=GAME_FIELD",
		function( xmlhttp )
		{
			document.getElementById("stdout").innerHTML = xmlhttp.responseText;
		});
}

function getChanges(mapId){
	ajaxRequest( 
		"game/viewData?gameid=" + mapId + "&type=PLAYERS_ACTIONS",
		function( xmlhttp )
		{
			document.getElementById("stdout").innerHTML = xmlhttp.responseText;
		});
}

function get(name){
   if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
      return decodeURIComponent(name[1]);
}

function ajaxRequest( requestURL, lamdaFunc )
{
	var xmlhttp;
	if (window.XMLHttpRequest)
	  {
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	var request = requestURL;
	xmlhttp.open("GET", request, true);
	xmlhttp.send();
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
			lamdaFunc( xmlhttp );
	    }
	};
}