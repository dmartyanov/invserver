
function updateTables(){
    loadRatings();
    loadGames();
}

//loading ratings
function loadRatings(){
    ajaxRequest("../replay/ratings",
        function( xmlhttp ){
            try {
                json = JSON.parse(xmlhttp.responseText);
            } catch(e) {
                window.clearInterval(initBlock.actionUpdater);
            }
            var textNode = null;
            var tableBody = document.getElementById("ratingTableBody");
            if(tableBody == null) return;
            var rNo = 1
            for(o in json){
                var tre = document.createElement("tr");
                var td1 = document.createElement("td");
                td1.setAttribute("class", "col-md-1");
                textNode  = document.createTextNode(rNo);
                td1.appendChild(textNode);
                var td2 = document.createElement("td");
                td2.setAttribute("class", "col-md-3");
                textNode  = document.createTextNode(json[o].userId);
                td2.appendChild(textNode);
                var td3 = document.createElement("td");
                td3.setAttribute("class", "col-md-1");
                textNode  = document.createTextNode(json[o].rating);
                td3.appendChild(textNode);
                var td4 = document.createElement("td");
                td4.setAttribute("class", "col-md-1");
                textNode = document.createTextNode(json[o].gamesNo);
                td4.appendChild(textNode);

                tre.appendChild(td1);
                tre.appendChild(td2);
                tre.appendChild(td3);
                tre.appendChild(td4);

                tableBody.appendChild(tre);
                rNo = rNo + 1;
            }
        }
    )
}

//load games
function loadGames(){
    ajaxRequest("../replay/allgames",
        function( xmlhttp ){
            try {
                json = JSON.parse(xmlhttp.responseText);
            } catch(e) {
                window.clearInterval(initBlock.actionUpdater);
            }
            var textNode = null;
            var tableBody = document.getElementById("gamesTableBody");
            if(tableBody == null) return;
            var rNo = 1
            for(o in json){
                var tre = document.createElement("tr");
                var td1 = document.createElement("td");
                td1.setAttribute("class", "col-md-1");
                textNode  = document.createTextNode(rNo);
                td1.appendChild(textNode);
                var td2 = document.createElement("td");
                td2.setAttribute("class", "col-md-2");
                textNode  = document.createTextNode(json[o].gameIdent);
                td2.appendChild(textNode);
                var td3 = document.createElement("td");
                td3.setAttribute("class", "col-md-2");
                textNode  = document.createTextNode(json[o].playersList);
                td3.appendChild(textNode);
                var td4 = document.createElement("td");
                td4.setAttribute("class", "col-md-2");
                textNode = document.createTextNode(json[o].gameWinner);
                td4.appendChild(textNode);
                var td5 = document.createElement("td");
                td5.setAttribute("class", "col-md-2");
                textNode = document.createTextNode(json[o].gameDate);
                td5.appendChild(textNode);

                tre.appendChild(td1);
                tre.appendChild(td2);
                tre.appendChild(td3);
                tre.appendChild(td4);
                tre.appendChild(td5);

                tableBody.appendChild(tre);
                rNo = rNo + 1;
            }
        }
    )
}

window.onload = updateTables;
