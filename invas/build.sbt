name := "invas"

version := "1.0"

scalaVersion := "2.11.4"

organization := "www.tcsbank.ru"

libraryDependencies ++=
    akka("2.3.7") ++
    spray("1.3.1") ++
    dbDeps ++
  Seq(
    "org.scala-lang"            % "scala-reflect"         % "2.11.4",
    "com.typesafe.scala-logging"  %% "scala-logging"        % "3.1.0",
    "org.json4s"                %%  "json4s-jackson"      % "3.2.10",
    "ch.qos.logback"            %   "logback-classic"     % "1.1.2",
    "org.scalatest"             %%  "scalatest"           % "2.2.2"     % "test"
  )

def akka(v: String) = Seq(
  "com.typesafe.akka" %% "akka-actor"   % v,
  "com.typesafe.akka" %% "akka-remote"  % v,
  "com.typesafe.akka" %% "akka-slf4j"   % v,
  "com.typesafe.akka" %% "akka-testkit" % v % "test"
)

def spray(v : String) = Seq(
  "io.spray"            %%   "spray-can"     % v,
  "io.spray"            %%   "spray-client"  % v,
  "io.spray"            %%   "spray-routing" % v,
  "io.spray"            %%   "spray-caching" % v,
  "io.spray"            %%   "spray-testkit" % v  % "test"
)

def dbDeps = Seq(
  "com.mchange"             %   "c3p0"                    % "0.9.2.1",
  "com.typesafe.slick"      %%  "slick"                   % "2.1.0",
  "mysql"                   % "mysql-connector-java"      % "5.1.34"
)